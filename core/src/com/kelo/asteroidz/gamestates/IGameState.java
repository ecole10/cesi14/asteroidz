package com.kelo.asteroidz.gamestates;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.kelo.asteroidz.entities.Player;

import java.util.ArrayList;

public interface IGameState {

    void init();
    void init(ArrayList<Player> players);
    void update(float dt);
    void update(float dt, ArrayList<Player> players);
    void draw();
    void draw(ArrayList<Player> players);
    void draw(ShapeRenderer sr, SpriteBatch sb, ArrayList<Player> players);
    void handleInput();
    void handleInput(Player player);
    void handleInput(ArrayList<Player> players);
    void dispose();
}
