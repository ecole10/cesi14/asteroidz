package com.kelo.asteroidz.gamestates;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.kelo.asteroidz.entities.Player;
import com.kelo.asteroidz.managers.GameStateManager;

import java.util.ArrayList;

public abstract class GameState implements IGameState{

    protected GameStateManager gsm;

    protected GameState(GameStateManager gsm) {
        this.gsm = gsm;
        init();
    }

    protected GameState(GameStateManager gsm, ArrayList<Player> players) {
        this.gsm = gsm;
        init(players);
    }

    public GameState(GameStateManager gsm, int totalPlayers) {
        this.gsm = gsm;
        PauseState.numberPlayers = totalPlayers;
        init();
    }

    @Override
    public void init(){}
    @Override
    public void init(ArrayList<Player> players){}
    @Override
    public void update(float dt){}
    @Override
    public void update(float dt, ArrayList<Player> players){}
    @Override
    public void handleInput(){}
    @Override
    public void handleInput(Player player){}
    @Override
    public void handleInput(ArrayList<Player> players){}
    @Override
    public void draw(){}
    @Override
    public void draw(ShapeRenderer sr, SpriteBatch sb, ArrayList<Player> players){}
    @Override
    public void draw(ArrayList<Player> players){}
}
