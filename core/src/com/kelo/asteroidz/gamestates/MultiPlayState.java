package com.kelo.asteroidz.gamestates;

import com.kelo.asteroidz.Asteroidz;
import com.kelo.asteroidz.entities.Laser;
import com.kelo.asteroidz.entities.Player;
import com.kelo.asteroidz.managers.GameKeys;
import com.kelo.asteroidz.managers.GameStateManager;

import java.util.ArrayList;

public class MultiPlayState extends PlayState {

    private static Player player1;
    private static Player player2;
    private ArrayList<Player> players;

    private static MultiPlayState instance = null;

    private MultiPlayState(GameStateManager gsm) {
        super(gsm);
    }

    public static MultiPlayState getInstance(GameStateManager gsm) {
        if(instance == null){
            instance = new MultiPlayState(gsm);
        }
        return instance;
    }

    public static MultiPlayState removeInstance() {
        if(instance != null){
            instance = null;
        }

        return instance;
    }

    @Override
    public void init() {
        super.init();
        ArrayList<Laser> lasersP1 = new ArrayList<Laser>();
        ArrayList<Laser> lasersP2 = new ArrayList<Laser>();
        players = new ArrayList<Player>();

        player1 = new Player();
        player1.setLasers(lasersP1);
        player1.setStartX(Asteroidz.WIDTH / 1.5f);
        player1.setStartY(Asteroidz.HEIGHT / 2);
        player1.setPosition(player1.getStartX(), player1.getStartY());

        player2 = new Player();
        player2.setLasers(lasersP2);
        player2.setStartX(Asteroidz.WIDTH / 4);
        player2.setStartY(Asteroidz.HEIGHT / 2);
        player2.setLasers(lasersP2);
        player2.setPosition(player2.getStartX(), player2.getStartY());
        players.add(player1);
        players.add(player2);

        super.setPlayers(players);
        super.init();
    }

    @Override
    public void draw() {
        super.draw();
    }

    @Override
    public void update(float dt) {
        handleInput(this.players);
        super.update(dt);
    }

    public static int totalLives() {
        return player1.getLives() + player2.getLives();
    }
    public static long totalScore() { return player1.getScore() + player2.getScore(); }

    @Override
    public void handleInput(ArrayList<Player> players) {
        super.handleInput(players.get(0));
        players.get(1).setUp(GameKeys.isDown(GameKeys.Z));
        players.get(1).setLeft(GameKeys.isDown(GameKeys.Q));
        players.get(1).setRight(GameKeys.isDown(GameKeys.D));
        if(GameKeys.isPressed(GameKeys.CTRL_L)) {
            players.get(1).shoot();
        }
    }

    @Override
    public void dispose() {
        super.dispose();
    }
}
