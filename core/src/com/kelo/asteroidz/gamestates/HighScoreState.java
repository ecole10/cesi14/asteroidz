package com.kelo.asteroidz.gamestates;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.kelo.asteroidz.Asteroidz;
import com.kelo.asteroidz.managers.GameKeys;
import com.kelo.asteroidz.managers.GameStateManager;
import com.kelo.asteroidz.managers.Save;

public class HighScoreState extends GameState {

    private SpriteBatch sb;

    private BitmapFont font;
    private BitmapFont titleFont;

    private long[] highScores;
    private String[] names;

    public HighScoreState(GameStateManager gsm) {
        super(gsm);
    }

    public void init() {

        sb = new SpriteBatch();

        FreeTypeFontGenerator gen = new FreeTypeFontGenerator(Gdx.files.internal("fonts/Hyperspace Bold.ttf"));

        FreeTypeFontParameter parameterTitle = new FreeTypeFontParameter();
        parameterTitle.size = Asteroidz.HEIGHT / 10;
        titleFont = gen.generateFont(parameterTitle);

        FreeTypeFontParameter parameter = new FreeTypeFontParameter();
        parameter.size = Asteroidz.HEIGHT / 20;
        font = gen.generateFont(parameter);

        Save.load();
        highScores = Save.gd.getHighScores();
        names = Save.gd.getNames();

    }

    public void update(float dt) {
        handleInput();
    }

    public void draw() {

        sb.setProjectionMatrix(Asteroidz.cam.combined);

        sb.begin();

        String title = "High Scores";

        GlyphLayout layout = new GlyphLayout();
        layout.setText(titleFont, title);

        titleFont.draw(sb, layout, (Asteroidz.WIDTH - layout.width) / 2, Asteroidz.HEIGHT - layout.height);

        for(int i = 0; i < highScores.length; i++) {
            String score = String.format(
                    "%2d. %7s %s",
                    i + 1,
                    highScores[i],
                    names[i]
            );
            layout.setText(font, score);
            font.draw(sb, layout, (Asteroidz.WIDTH - layout.width) / 2, (Asteroidz.HEIGHT / 2 + Asteroidz.HEIGHT / 5) - (Asteroidz.HEIGHT / 20) * i);
        }

        sb.end();

    }

    public void handleInput() {
        if(GameKeys.isPressed(GameKeys.ENTER) || GameKeys.isPressed(GameKeys.ESCAPE)) {
            gsm.setState(GameStateManager.MENU);
        }
    }

    public void dispose() {
        sb.dispose();
        font.dispose();
    }
}
