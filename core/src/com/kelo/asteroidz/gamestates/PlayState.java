package com.kelo.asteroidz.gamestates;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.kelo.asteroidz.Asteroidz;
import com.kelo.asteroidz.entities.*;
import com.kelo.asteroidz.managers.GameKeys;
import com.kelo.asteroidz.managers.GameStateManager;
import com.kelo.asteroidz.managers.Jukebox;
import com.kelo.asteroidz.managers.Save;

import java.util.ArrayList;
import java.util.HashMap;

public class PlayState extends GameState {

    private SpriteBatch sb;
    private ShapeRenderer sr;

    private BitmapFont font;
    private PlayerLife hudPlayer;

    private Player player;

    private ArrayList<Player> players;
    private ArrayList<Asteroid> asteroids;
    private ArrayList<Bullet> enemyBullets;

    private SuperAsteroid superAsteroid;

    private FlyingSaucer flyingSaucer;
    private float fsTimerFlyingSaucer;
    private float fsTimeFlyingSaucer;

    private float fsTimerSuperAsteroids;
    private float fsTimeSuperAsteroids;

    private ArrayList<Particle> particles;

    private int level;
    private int totalAsteroids;
    private int numAsteroidsLeft;
    private long totalScore = 0;
    private int totalPlayerLives;

    public static int totalPlayers;
    HashMap<String, Sprite> spacecrafts;

    private float maxDelay;
    private float minDelay;
    private float currentDelay;
    private float bgTimer;
    private boolean playLowPulse;

    private static PlayState instance = null;


    protected PlayState(GameStateManager gsm) {
        super(gsm);
    }

    public static PlayState getInstance(GameStateManager gsm) {
        if(instance == null){
            instance = new PlayState(gsm);
        }
        return instance;
    }

    public static PlayState removeInstance() {
        if(instance != null){
            instance = null;
        }

        return instance;

    }

    public void setPlayers(ArrayList<Player> players) {
        this.players = players;
    }

    public void setTotalLives(int lives) {
        this.totalPlayerLives = lives;
    }
    public static int getTotalPlayers() {
        return totalPlayers;
    }

    @Override
    public void init() {

        sb = new SpriteBatch();
        sr = new ShapeRenderer();

        FreeTypeFontGenerator gen = new FreeTypeFontGenerator(Gdx.files.internal("fonts/Hyperspace Bold.ttf"));
        FreeTypeFontParameter parameter = new FreeTypeFontParameter();
        parameter.size = 12;
        font = gen.generateFont(parameter);

        ArrayList<Laser> lasers = new ArrayList<Laser>();

        if (players == null) {
            player = new Player();
            setTotalLives(player.getLives());
            player.setLasers(lasers);
            players = new ArrayList<Player>();
            players.add(player);
            setPlayers(players);
        }

        asteroids = new ArrayList<Asteroid>();

        particles = new ArrayList<Particle>();

        level = 1;
        spawnAsteroids();

        spacecrafts = player.getSpacecrafts();

        if (players != null && players.size() > 0) {

            for (int i = 0; i < players.size(); i++) {
                hudPlayer = new PlayerLife();
            }

            totalPlayers = players.size();
        }


        fsTimerFlyingSaucer = 0;
        fsTimeFlyingSaucer = 10;

        fsTimerSuperAsteroids = 0;
        fsTimeSuperAsteroids = 15;

        enemyBullets = new ArrayList<Bullet>();

        // set up bg music
        maxDelay = 1;
        minDelay = 0.25f;
        currentDelay = maxDelay;
        bgTimer = maxDelay;
        playLowPulse = true;

    }

    private void createParticles(float x, float y) {
        for(int i = 0; i < 6; i++) {
            particles.add(new Particle(x, y));
        }
    }

    private void splitAsteroids(Asteroid a) {
        createParticles(a.getx(), a.gety());
        numAsteroidsLeft--;
        currentDelay = ((maxDelay - minDelay) * numAsteroidsLeft / totalAsteroids) + minDelay;
        if(a.getType() == Asteroid.LARGE) {
            asteroids.add(
                    new Asteroid(a.getx(), a.gety(), Asteroid.MEDIUM));
            asteroids.add(
                    new Asteroid(a.getx(), a.gety(), Asteroid.MEDIUM));
        }
        if(a.getType() == Asteroid.MEDIUM) {
            asteroids.add(
                    new Asteroid(a.getx(), a.gety(), Asteroid.SMALL));
            asteroids.add(
                    new Asteroid(a.getx(), a.gety(), Asteroid.SMALL));
        }
    }

    private void destroyAll() {
        for (Asteroid a : asteroids) {
            createParticles(a.getx(), a.gety());
        }
        asteroids.clear();
    }

    private void spawnAsteroids() {

        asteroids.clear();

        int numToSpawn = 4 + level - 1;
        totalAsteroids = numToSpawn * 7;
        numAsteroidsLeft = totalAsteroids;
        currentDelay = maxDelay;

        for(int i = 0; i < numToSpawn; i++) {

            float x = MathUtils.random(Asteroidz.WIDTH);
            float y = MathUtils.random(Asteroidz.HEIGHT);

            float dx = x - player.getx();
            float dy = y - player.gety();
            float dist = (float) Math.sqrt(dx * dx + dy * dy);

            while(dist < 100) {
                x = MathUtils.random(Asteroidz.WIDTH);
                y = MathUtils.random(Asteroidz.HEIGHT);
                for (Player player : players) {
                    dx = x - player.getx();
                    dy = y - player.gety();
                }
                dist = (float) Math.sqrt(dx * dx + dy * dy);
            }

            asteroids.add(new Asteroid(x, y, Asteroid.LARGE));

        }
    }

    @Override
    public void update(float dt) {

        // get user input
        handleInput(player);

        // next level
        if(asteroids.size() == 0) {
            Jukebox.stop("largesaucer");
            Jukebox.stop("smallsaucer");
            Jukebox.stop("invincible");
            level++;
            flyingSaucer = null;
            superAsteroid = null;
            for (Player player : players) {
                if (player.canUsePower()) {
                    player.resetPower();
                }
            }
            spawnAsteroids();
        }

        // update player
        for (Player player : players) {
            player.update(dt);
            if (player.isDead()) {
                if (player.getLives() == 0) {
                    Jukebox.stopAll();
                    Save.gd.setTenativeScore(player.getScore());
                    gsm.setState(GameStateManager.GAMEOVER);
                    return;
                }
                player.reset(player.getStartX(), player.getStartY());
                player.loseLife();
                flyingSaucer = null;
                superAsteroid = null;
                Jukebox.stop("smallsaucer");
                Jukebox.stop("largesaucer");
                return;
            }
        }

        // update player lasers
        for (Player player : players) {
            for (int i = 0; i < player.getLasers().size(); i++) {
                  player.getLasers().get(i).update(dt);
                if (player.getLasers().get(i).shouldRemove()) {
                    player.getLasers().remove(i);
                    i--;
                }
            }
        }

        // update flying saucer
        if(flyingSaucer == null) {

            fsTimerFlyingSaucer += dt;
            if(fsTimerFlyingSaucer >= fsTimeFlyingSaucer) {
                fsTimerFlyingSaucer = 0;
                int type = MathUtils.random() < 0.5 ?
                        FlyingSaucer.SMALL : FlyingSaucer.LARGE;
                int direction = MathUtils.random() < 0.5 ?
                        FlyingSaucer.RIGHT : FlyingSaucer.LEFT;
                flyingSaucer = new FlyingSaucer(
                        type,
                        direction,
                        players,
                        enemyBullets
                );
            }
        }
        // if there is a flying saucer already
        else {
            flyingSaucer.update(dt);
            if(flyingSaucer.shouldRemove()) {
                flyingSaucer = null;
                Jukebox.stop("smallsaucer");
                Jukebox.stop("largesaucer");
            }
        }

        // update fs bullets
        for(int i = 0; i < enemyBullets.size(); i++) {
            enemyBullets.get(i).update(dt);
            if(enemyBullets.get(i).shouldRemove()) {
                enemyBullets.remove(i);
                i--;
            }
        }

        // update asteroids
        for(int i = 0; i < asteroids.size(); i++) {
            asteroids.get(i).update(dt);
            if(asteroids.get(i).shouldRemove()) {
                asteroids.remove(i);
                i--;
            }
        }

        // update super asteroids
        if(superAsteroid == null) {

            fsTimerSuperAsteroids += dt;
            if(fsTimerSuperAsteroids >= fsTimeSuperAsteroids) {
                fsTimerSuperAsteroids = 0;
                int type = Asteroid.MEDIUM;
                float x = MathUtils.random(Asteroidz.WIDTH);
                float y = MathUtils.random(Asteroidz.HEIGHT);
                superAsteroid = new SuperAsteroid(x, y, type);
            }
        }
        // if there is a super asteroids already
        else {
            superAsteroid.update(dt);
            if(superAsteroid.shouldRemove()) {
                superAsteroid = null;
            }
        }


        // update particles
        for(int i = 0; i < particles.size(); i++) {
            particles.get(i).update(dt);
            if(particles.get(i).shouldRemove()) {
                particles.remove(i);
                i--;
            }
        }

        // check collision
        checkCollisions();

        // play bg music
        bgTimer += dt;
        if(!player.isHit() && bgTimer >= currentDelay && !player.usedPower()) {
            if(playLowPulse) {
                Jukebox.play("pulselow");
            }
            else {
                Jukebox.play("pulsehigh");
            }
            playLowPulse = !playLowPulse;
            bgTimer = 0;
        }
    }

    private void checkCollisions() {

        // player-asteroid collision
        for (Player player : players) {
            if (!player.isHit()) {
                for (int i = 0; i < asteroids.size(); i++) {
                    Asteroid a = asteroids.get(i);
                    if (!player.isDisableCollision()) {
                        if (a.intersects(player)) {
                            if (!player.usedPower()) {
                                player.hit();
                            }
                            asteroids.remove(i);
                            i--;
                            splitAsteroids(a);
                            Jukebox.play("explode");
                            break;
                        }
                    }
                }
            }

            // bullet-asteroid collision
            for (int i = 0; i < player.getLasers().size(); i++) {
                Bullet b = player.getLasers().get(i);
                for (int j = 0; j < asteroids.size(); j++) {
                    Asteroid a = asteroids.get(j);
                    if (a.intersects(b)) {
                        player.getLasers().remove(i);
                        i--;

                        asteroids.remove(j);
                        j--;
                        splitAsteroids(a);
                        player.incrementScore(a.getScore());
                        Jukebox.play("explode");
                        break;
                    }
                }
            }

            // player-flying saucer collision
            if (flyingSaucer != null) {
                if (!player.isDisableCollision()) {
                    if (player.intersects(flyingSaucer)) {
                        if (!player.usedPower()) {
                            player.hit();
                        }
//                        createParticles(player.getx(), player.gety());
                        createParticles(flyingSaucer.getx(), flyingSaucer.gety());
                        flyingSaucer = null;
                        Jukebox.stop("smallsaucer");
                        Jukebox.stop("largesaucer");
                        Jukebox.play("explode");
                    }
                }
            }

            // bullet-flying saucer collision
            if (flyingSaucer != null) {
                for (int i = 0; i < player.getLasers().size(); i++) {
                    Bullet b = player.getLasers().get(i);
                    if (flyingSaucer.intersects(b)) {
                        player.getLasers().remove(i);
                        i--;
                        createParticles(
                                flyingSaucer.getx(),
                                flyingSaucer.gety()
                        );
                        player.incrementScore(flyingSaucer.getScore());
                        player.incrementKilledFlyingSaucer();
                        flyingSaucer = null;
                        Jukebox.stop("smallsaucer");
                        Jukebox.stop("largesaucer");
                        Jukebox.play("explode");
                        break;
                    }
                }
            }

            // player-super asteroid collision
            if (superAsteroid != null) {
                if (!player.isDisableCollision()) {
                    if (player.intersects(superAsteroid)) {
                        if (!player.usedPower()) {
                            player.hit();
                        }
//                        createParticles(player.getx(), player.gety());
                        createParticles(superAsteroid.getx(), superAsteroid.gety());
                        superAsteroid = null;
                        Jukebox.play("explode");
                    }
                }
            }

            // bullet-super asteroid collision
            if (superAsteroid != null) {
                for (int i = 0; i < player.getLasers().size(); i++) {
                    Bullet b = player.getLasers().get(i);
                    if (superAsteroid.intersects(b)) {
                        player.getLasers().remove(i);
                        i--;
                        createParticles(
                                superAsteroid.getx(),
                                superAsteroid.gety()
                        );
                        for (Asteroid a : asteroids) a.freeze(a.getdx(), a.getdy(), superAsteroid.getTimeFreeze());
                        if (flyingSaucer != null) {
                            flyingSaucer.freeze(flyingSaucer.getdx(), flyingSaucer.getdy(), superAsteroid.getTimeFreeze());
                        }
                        superAsteroid = null;
                        Jukebox.play("rewind");
                        break;
                    }
                }
            }

            // player-enemy bullets collision
            if (!player.isHit()) {
                for (int i = 0; i < enemyBullets.size(); i++) {
                    Bullet b = enemyBullets.get(i);
                    if (!player.isDisableCollision()) {
                        if (player.intersects(b)) {
                            if (!player.usedPower()) {
                                player.hit();
                            }
                            enemyBullets.remove(i);
                            i--;
                            Jukebox.play("explode");
                            break;
                        }
                    }
                }
            }

            // flying saucer-asteroid collision
            if (flyingSaucer != null) {
                for (int i = 0; i < asteroids.size(); i++) {
                    Asteroid a = asteroids.get(i);
                    if (a.intersects(flyingSaucer)) {
                        asteroids.remove(i);
                        i--;
                        splitAsteroids(a);
                        createParticles(a.getx(), a.gety());
                        createParticles(
                                flyingSaucer.getx(),
                                flyingSaucer.gety()
                        );
                        flyingSaucer = null;
                        Jukebox.stop("smallsaucer");
                        Jukebox.stop("largesaucer");
                        Jukebox.play("explode");
                        break;
                    }
                }
            }

            // asteroid-enemy bullet collision
            for (int i = 0; i < enemyBullets.size(); i++) {
                Bullet b = enemyBullets.get(i);
                for (int j = 0; j < asteroids.size(); j++) {
                    Asteroid a = asteroids.get(j);
                    if (a.intersects(b)) {
                        asteroids.remove(j);
                        j--;
                        splitAsteroids(a);
                        enemyBullets.remove(i);
                        i--;
                        createParticles(a.getx(), a.gety());
                        Jukebox.play("explode");
                        break;
                    }
                }
            }
        }
    }

    private void drawObjects(ArrayList<? extends SpaceObject> objects, ShapeRenderer sr) {
        for (SpaceObject object : objects) {
            object.draw(sr);
        }
    }

    private void drawLifes(int i) {
        Sprite life = spacecrafts.get("player");
        life.setPosition((Asteroidz.WIDTH / 15) - (Asteroidz.WIDTH / 20) + i * 15,Asteroidz.HEIGHT - 60);
        life.setRotation(0);
        hudPlayer.setPlayer(life);
        hudPlayer.draw(sb);
    }

    @Override
    public void draw(ArrayList<Player> players){
        for (Player player : players) {
            player.draw(sb);
        }

    }

    @Override
    public void draw() {

        sb.setProjectionMatrix(Asteroidz.cam.combined);
        sr.setProjectionMatrix(Asteroidz.cam.combined);

        // draw player
        draw(players);

        // draw bullets
        for (Player player : players) {drawObjects(player.getLasers(), sr);}

        // draw flying saucer
        if(flyingSaucer != null) {
            flyingSaucer.draw(sb);
        }

        // draw fs bullets
        drawObjects(enemyBullets, sr);

        // draw asteroids
        for (Asteroid asteroid : asteroids) {
            asteroid.draw(sb);
        }

        // draw super asteroid
        if(superAsteroid != null) {
            superAsteroid.draw(sb);
        }

        // draw particles
        drawObjects(particles, sr);

        // draw score
        sb.setColor(1, 1, 1, 1);
        sb.begin();
        if (players.size() == 1) {
            font.draw(sb, Long.toString(player.getScore()), (Asteroidz.WIDTH / 12) - (Asteroidz.WIDTH / 20),
                    Asteroidz.HEIGHT - 10);
            sb.end();
        } else {
            font.draw(sb, Long.toString(MultiPlayState.totalScore()), (Asteroidz.WIDTH / 12) - (Asteroidz.WIDTH / 20),
                    Asteroidz.HEIGHT - 10);
            sb.end();
        }

        // draw level
        sb.setColor(1, 1, 1, 1);
        sb.begin();
        font.draw(sb, "Level " + level, Asteroidz.WIDTH - 80, Asteroidz.HEIGHT - 10);
        sb.end();

        // draw lives
        if (players.size() == 1) {
            for (int i = 0; i < player.getLives(); i++) {
                drawLifes(i);
            }
        } else {
            for (int i = 0; i < MultiPlayState.totalLives(); i++) {
                drawLifes(i);
            }
        }

        // draw flying saucer killed
        Texture flyingSaucer = new Texture(Gdx.files.internal("images/flying_saucer.png"));
        sb.setColor(1, 1, 1, 1);
        sb.begin();

        if(players.get(0).getPower() == 0) {
            Texture flyingSaucerPowerInvincible = new Texture(Gdx.files.internal("images/flying_saucer_power_invincible.png"));
            sb.draw(flyingSaucerPowerInvincible, (Asteroidz.WIDTH / 12) - (Asteroidz.WIDTH / 20) + 12, Asteroidz.HEIGHT - 80);
        } else if (players.get(0).getPower() == 1) {
            Texture flyingSaucerPowerDestroy = new Texture(Gdx.files.internal("images/flying_saucer_power_destroy.png"));
            sb.draw(flyingSaucerPowerDestroy, (Asteroidz.WIDTH / 12) - (Asteroidz.WIDTH / 20) + 12, Asteroidz.HEIGHT - 80);
        } else {
            sb.draw(flyingSaucer, (Asteroidz.WIDTH / 12) - (Asteroidz.WIDTH / 20) + 12, Asteroidz.HEIGHT - 80);
        }

        font.draw(sb, Integer.toString(players.get(0).getNextPower() - players.get(0).getKilledFlyingSaucer()),
                (Asteroidz.WIDTH / 12) - (Asteroidz.WIDTH / 20), Asteroidz.HEIGHT - 60);
        sb.end();
    }

    @Override
    public void handleInput(Player player) {

        if(!player.isHit()) {
            player.setLeft(GameKeys.isDown(GameKeys.LEFT));
            player.setRight(GameKeys.isDown(GameKeys.RIGHT));
            player.setUp(GameKeys.isDown(GameKeys.UP));
            if(GameKeys.isPressed(GameKeys.SPACE)) {
                player.shoot();
            }
            if(player.canUsePower()) {
                if(GameKeys.isPressed(GameKeys.SHIFT)) {
                    if (player.usePower(player.getPower())) {
                        Jukebox.play("destroy");
                        destroyAll();
                        player.setPlayer(spacecrafts.get("player"));
                        player.resetPower();
                    }
                    player.incrementNextPower(players.size());
                }
            }
        }

        if(GameKeys.isPressed(GameKeys.ESCAPE)) {
            gsm.setState(GameStateManager.PAUSE);
        }
    }

    @Override
    public void dispose() {
        sb.dispose();
        sr.dispose();
        font.dispose();
    }
}
