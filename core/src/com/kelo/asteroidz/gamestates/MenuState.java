package com.kelo.asteroidz.gamestates;

import java.util.ArrayList;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.kelo.asteroidz.entities.Asteroid;
import com.kelo.asteroidz.Asteroidz;
import com.kelo.asteroidz.entities.Bullet;
import com.kelo.asteroidz.entities.FlyingSaucer;
import com.kelo.asteroidz.entities.Player;
import com.kelo.asteroidz.managers.GameKeys;
import com.kelo.asteroidz.managers.GameStateManager;
import com.kelo.asteroidz.managers.Jukebox;
import com.kelo.asteroidz.managers.Save;

public class MenuState extends GameState {

    private SpriteBatch sb;
    private ShapeRenderer sr;

    private BitmapFont font;
    private BitmapFont titleFont;

    private final String title = "Asteroidz";

    private int currentItem;
    private String[] menuItems;

    private ArrayList<Asteroid> asteroids;

    public MenuState(GameStateManager gsm) {
        super(gsm);
    }

    public void init() {

        sb = new SpriteBatch();
        sr = new ShapeRenderer();

        FreeTypeFontGenerator gen = new FreeTypeFontGenerator(Gdx.files.internal("fonts/Hyperspace Bold.ttf"));

        FreeTypeFontParameter parameterTitle = new FreeTypeFontParameter();
        parameterTitle.size = Asteroidz.HEIGHT / 8;
        parameterTitle.color = Color.WHITE;
        titleFont = gen.generateFont(parameterTitle);

        FreeTypeFontParameter parameter = new FreeTypeFontParameter();
        parameter.size = Asteroidz.HEIGHT / 14;
        font = gen.generateFont(parameter);

        menuItems = new String[] {
                "Play solo",
                "Multiplayer",
                "Highscores",
                "Controls",
                "Options",
                "Quit"
        };

        asteroids = new ArrayList<Asteroid>();
        for(int i = 0; i < 6; i++) {
            asteroids.add(
                    new Asteroid(
                            MathUtils.random(Asteroidz.WIDTH),
                            MathUtils.random(Asteroidz.HEIGHT),
                            Asteroid.LARGE
                    )
            );
        }

        Save.load();

        Jukebox.init(Save.gd.getSounds(), Save.gd.getMusics());
        Jukebox.loop("soundtrack");
        Jukebox.play("soundtrack");
    }

    public void update(float dt) {

        handleInput();

        for(int i = 0 ; i < asteroids.size(); i++) {
            asteroids.get(i).update(dt);
        }

    }

    public void draw() {

        sb.setProjectionMatrix(Asteroidz.cam.combined);
        sr.setProjectionMatrix(Asteroidz.cam.combined);

        // draw asteroids
        for(int i = 0; i < asteroids.size(); i++) {
            asteroids.get(i).draw(sb);
        }

        sb.begin();

        // draw title
        GlyphLayout layout = new GlyphLayout();
        layout.setText(titleFont, title);

        titleFont.draw(sb, layout, (Asteroidz.WIDTH - layout.width) / 2, Asteroidz.HEIGHT - layout.height);

        // draw menu
        for(int i = 0; i < menuItems.length; i++) {
            if(currentItem == i) font.setColor(Color.RED);
            else font.setColor(Color.WHITE);
            layout.setText(font, menuItems[i]);
            font.draw(sb, layout, (Asteroidz.WIDTH - layout.width) / 2, (Asteroidz.HEIGHT / 1.5f) - (layout.height + 20) * i);
        }

        sb.end();

    }

    public void handleInput() {

        if(GameKeys.isPressed(GameKeys.UP)) {
            if(currentItem > 0) {
                currentItem--;
            }
        }
        if(GameKeys.isPressed(GameKeys.DOWN)) {
            if(currentItem < menuItems.length - 1) {
                currentItem++;
            }
        }
        if(GameKeys.isPressed(GameKeys.ENTER)) {
            select();
        }

    }

    private void select() {
        // play
        if(currentItem == 0) {
            Jukebox.stop("soundtrack");
            gsm.setState(GameStateManager.PLAY);
        }
        if(currentItem == 1) {
            Jukebox.stop("soundtrack");
            gsm.setState(GameStateManager.MULTIPLAY);
        }
        // high scores
        else if(currentItem == 2) {
            gsm.setState(GameStateManager.HIGHSCORE);
        }
        // parameter
        else if(currentItem == 3) {
            gsm.setState(GameStateManager.CONTROLS);
        }
        else if(currentItem == 4) {
            gsm.setState(GameStateManager.OPTIONS);
        }
        else if(currentItem == 5) {
            Gdx.app.exit();
        }
    }

    public void dispose() {
        PlayState.removeInstance();
        MultiPlayState.removeInstance();
        sb.dispose();
        sr.dispose();
        titleFont.dispose();
        font.dispose();
    }

}
