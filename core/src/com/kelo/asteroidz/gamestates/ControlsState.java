package com.kelo.asteroidz.gamestates;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.kelo.asteroidz.Asteroidz;
import com.kelo.asteroidz.entities.Player;
import com.kelo.asteroidz.managers.GameKeys;
import com.kelo.asteroidz.managers.GameStateManager;

import java.util.ArrayList;

public class ControlsState extends GameState {

    private SpriteBatch sb;

    private BitmapFont titleFont;
    private BitmapFont subTitleFont;
    private BitmapFont font;

    private String[] controlsKeysPlayerOne = new String[] {
            "UP",
            "LEFT",
            "RIGHT",
            "SPACE",
            "SHIFT"
    }, controlsKeysPlayerTwo = new String[] {
            "Z",
            "Q",
            "D",
            "CTRL",
            "SHIFT"
    }, controlsActions = new String[] {
            "Accelerate",
            "Turn left",
            "Turn right",
            "Shoot",
            "Activate power"
    };

    public ControlsState(GameStateManager gsm) {
        super(gsm);
    }

    @Override
    public void init() {

        sb = new SpriteBatch();

        FreeTypeFontGenerator gen = new FreeTypeFontGenerator(Gdx.files.internal("fonts/Hyperspace Bold.ttf"));

        FreeTypeFontGenerator.FreeTypeFontParameter parameterTitle = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameterTitle.size = Asteroidz.HEIGHT / 15;
        titleFont = gen.generateFont(parameterTitle);

        FreeTypeFontGenerator.FreeTypeFontParameter parameterSubTitle = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameterSubTitle.size = Asteroidz.HEIGHT / 20;
        subTitleFont = gen.generateFont(parameterSubTitle);

        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = Asteroidz.HEIGHT / 30;
        font = gen.generateFont(parameter);

    }

    @Override
    public void update(float dt) {handleInput();}

    @Override
    public void draw() {

        sb.setProjectionMatrix(Asteroidz.cam.combined);

        sb.begin();

        GlyphLayout layout = new GlyphLayout();

        String title = "Controls";
        layout.setText(titleFont, title);
        titleFont.draw(sb, layout, (Asteroidz.WIDTH - layout.width) / 2, Asteroidz.HEIGHT - layout.height);

        String subtitlePlayerOne = "Player One";
        layout.setText(subTitleFont, subtitlePlayerOne);
        subTitleFont.draw(sb, layout, (Asteroidz.WIDTH - layout.width) / 6, Asteroidz.HEIGHT / 1.5f);

        for(int i = 0; i < controlsKeysPlayerOne.length; i++) {
            String control = String.format("%5s", controlsKeysPlayerOne[i]);
            font.setColor(Color.WHITE);
            layout.setText(font, control);
            font.draw(sb, layout, (Asteroidz.WIDTH - layout.width) / 5, (Asteroidz.HEIGHT / 1.8f) - (layout.height + 20) * i);
        }

        for(int i = 0; i < controlsActions.length; i++) {
            String control = String.format("%15s", controlsActions[i]);
            font.setColor(Color.WHITE);
            layout.setText(font, control);
            font.draw(sb, layout, (Asteroidz.WIDTH - layout.width) / 2, (Asteroidz.HEIGHT / 1.8f) - (layout.height + 20) * i);
        }

        String subtitlePlayerTwo = "Player Two";
        layout.setText(subTitleFont, subtitlePlayerTwo);
        subTitleFont.draw(sb, layout, (Asteroidz.WIDTH - layout.width) / 1.1f, Asteroidz.HEIGHT / 1.5f);

        for(int i = 0; i < controlsKeysPlayerTwo.length; i++) {
            String control = String.format("%5s", controlsKeysPlayerTwo[i]);
            font.setColor(Color.WHITE);
            layout.setText(font, control);
            font.draw(sb, layout, (Asteroidz.WIDTH - layout.width) / 1.2f, (Asteroidz.HEIGHT / 1.8f) - (layout.height + 20) * i);
        }

        sb.end();
    }

    @Override
    public void draw(ArrayList<Player> players){}

    @Override
    public void handleInput() {
        if(GameKeys.isPressed(GameKeys.ENTER) || GameKeys.isPressed(GameKeys.ESCAPE)) {
            gsm.setState(GameStateManager.MENU);
        }
    }

    @Override
    public void dispose() {
        sb.dispose();
        font.dispose();
        titleFont.dispose();
    }
}
