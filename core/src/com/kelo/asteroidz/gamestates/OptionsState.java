package com.kelo.asteroidz.gamestates;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Align;
import com.kelo.asteroidz.Asteroidz;
import com.kelo.asteroidz.managers.*;
import java.util.HashMap;

public class OptionsState extends GameState {

    private SpriteBatch sb;
    private Stage stage;
    private Skin skin;

    private BitmapFont font;
    private BitmapFont titleFont;

    private Slider volumeSound;
    private Slider volumeMusic;
    private HashMap<String, Texture> volume;
    private GameInputProcessor GameInputProcessor;

    public OptionsState(GameStateManager gsm) {
        super(gsm);
    }

    public void init() {

        sb = new SpriteBatch();

        GameInputProcessor = new GameInputProcessor();

        FreeTypeFontGenerator gen = new FreeTypeFontGenerator(Gdx.files.internal("fonts/Hyperspace Bold.ttf"));

        FreeTypeFontGenerator.FreeTypeFontParameter parameterTitle = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameterTitle.size = Asteroidz.HEIGHT / 10;
        titleFont = gen.generateFont(parameterTitle);

        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = Asteroidz.HEIGHT / 30;
        font = gen.generateFont(parameter);

        volume = new HashMap<String, Texture>();

        volume.put("soundVolumeUp", new Texture(Gdx.files.internal("images/volume_up.png")));
        volume.put("soundVolumeDown", new Texture(Gdx.files.internal("images/volume_down.png")));
        volume.put("musicVolumeUp", new Texture(Gdx.files.internal("images/volume_up.png")));
        volume.put("musicVolumeDown", new Texture(Gdx.files.internal("images/volume_down.png")));

        Save.load();
        Float volumeSounds = Save.gd.getSounds();
        Float volumeMusics = Save.gd.getMusics();

        skin = new Skin(Gdx.files.internal("data/uiskin.json"));
        stage = new Stage();

        volumeSound = new Slider(0f, 1f, 0.1f, false, skin);
        if(volumeSounds != null) {
            volumeSound.setValue(volumeSounds);
        } else {
            volumeSound.setValue(1);
        }

        volumeMusic = new Slider(0f, 1f, 0.1f, false, skin);
        if(volumeMusics != null) {
            volumeMusic.setValue(volumeMusics);
        } else {
            volumeMusic.setValue(1);
        }

        final Label volumeValueSound = new Label(String.format("%.1f", volumeSound.getValue()), skin);
        final Label volumeValueMusic = new Label(String.format("%.1f", volumeMusic.getValue()), skin);

        final Table table = new Table();
        table.setFillParent(true);

        table.align(Align.center);
        table.row().padTop(30);
        table.add(volumeSound);
        table.add(volumeValueSound);
        table.row().padTop(50);
        table.add(volumeMusic);
        table.add(volumeValueMusic);
        stage.addActor(table);

        volumeSound.addListener(new ChangeListener() {
            public void changed (ChangeEvent event, Actor actor) {
                volumeValueSound.setText(String.format("%.1f",volumeSound.getValue()));

                Save.gd.addVolumeSounds(volumeSound.getValue());
                Save.save();

                Jukebox.init(volumeSound.getValue(), volumeMusic.getValue());
            }
        });

        volumeMusic.addListener(new ChangeListener() {
            public void changed (ChangeEvent event, Actor actor) {
                volumeValueMusic.setText(String.format("%.1f", volumeMusic.getValue()));

                Save.gd.addVolumeMusics(volumeMusic.getValue());
                Save.save();

                Jukebox.init(volumeSound.getValue(), volumeMusic.getValue());
            }
        });

    }

    public void update(float dt) {
        handleInput();
    }

    public void draw() {

        sb.setProjectionMatrix(Asteroidz.cam.combined);

        sb.begin();

        String title = "Options";

        GlyphLayout layout = new GlyphLayout();
        layout.setText(titleFont, title);

        titleFont.draw(sb, layout, (Asteroidz.WIDTH - layout.width) / 2, Asteroidz.HEIGHT - layout.height);

        String volumeSoundLabel = "Volume son";

        layout.setText(font, volumeSoundLabel);

        font.draw(sb, layout, (Asteroidz.WIDTH - layout.width) / 2, Asteroidz.HEIGHT / 2 + 80);

        if(volumeSound.getValue() == 0) {
            sb.draw(volume.get("soundVolumeDown"), (Asteroidz.WIDTH - layout.width) / 2 + 230, Asteroidz.HEIGHT / 2 + 55);
        } else {
            sb.draw(volume.get("soundVolumeUp"), (Asteroidz.WIDTH - layout.width) / 2 + 230, Asteroidz.HEIGHT / 2 + 55);
        }

        String volumeMusicLabel = "Volume musique";

        layout.setText(font, volumeMusicLabel);

        font.draw(sb, layout, (Asteroidz.WIDTH - layout.width) / 2, Asteroidz.HEIGHT / 2);

        if(volumeMusic.getValue() == 0) {
            sb.draw(volume.get("musicVolumeDown"), (Asteroidz.WIDTH - layout.width) / 2 + 320, Asteroidz.HEIGHT / 2 - 25);
        } else {
            sb.draw(volume.get("musicVolumeUp"), (Asteroidz.WIDTH - layout.width) / 2 + 320, Asteroidz.HEIGHT / 2 - 25);
        }

        sb.end();

        sb.begin();

        stage.act(Gdx.graphics.getDeltaTime());
        stage.draw();

        sb.end();

    }

    @Override
    public void handleInput() {

        InputMultiplexer multiplexer = new InputMultiplexer();
        Gdx.input.setInputProcessor(multiplexer);
        multiplexer.addProcessor(GameInputProcessor);
        multiplexer.addProcessor(stage);

        if(GameKeys.isPressed(GameKeys.ENTER) || GameKeys.isPressed(GameKeys.ESCAPE)) {
            gsm.setState(GameStateManager.MENU);
        }

    }

    @Override
    public void dispose() {
        stage.dispose();
        skin.dispose();
        sb.dispose();
        font.dispose();
    }
}
