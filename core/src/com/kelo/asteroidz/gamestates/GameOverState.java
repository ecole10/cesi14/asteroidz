package com.kelo.asteroidz.gamestates;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.kelo.asteroidz.Asteroidz;
import com.kelo.asteroidz.managers.GameKeys;
import com.kelo.asteroidz.managers.GameStateManager;
import com.kelo.asteroidz.managers.Jukebox;
import com.kelo.asteroidz.managers.Save;

public class GameOverState extends GameState {

    private SpriteBatch sb;
    private ShapeRenderer sr;

    private boolean newHighScore;
    private char[] newName;
    private int currentChar;

    private BitmapFont gameOverFont;
    private BitmapFont font;

    public GameOverState(GameStateManager gsm) {
        super(gsm);
    }

    public void init() {

        sb = new SpriteBatch();
        sr = new ShapeRenderer();

        newHighScore = Save.gd.isHighScore(Save.gd.getTentativeScore());
        if(newHighScore) {
            newName = new char[] {'A', 'A', 'A'};
            currentChar = 0;
        }

        FreeTypeFontGenerator gen = new FreeTypeFontGenerator(Gdx.files.internal("fonts/Hyperspace Bold.ttf"));

        FreeTypeFontGenerator.FreeTypeFontParameter parameterTitle = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameterTitle.size = Asteroidz.HEIGHT / 9;
        parameterTitle.color = Color.RED;
        gameOverFont = gen.generateFont(parameterTitle);

        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = 20;
        font = gen.generateFont(parameter);

        Jukebox.play("gameover");
    }

    public void update(float dt) {
        handleInput();
    }

    public void draw() {

        sb.setProjectionMatrix(Asteroidz.cam.combined);

        sb.begin();

        String title = "Game Over";

        GlyphLayout layout = new GlyphLayout();
        layout.setText(gameOverFont, title);

        gameOverFont.draw(sb, layout, (Asteroidz.WIDTH - layout.width) / 2, Asteroidz.HEIGHT / 2f + layout.height);

        if(!newHighScore) {
            sb.end();
            return;
        }

        String newScore = "New High Score: " + Save.gd.getTentativeScore();
        layout.setText(font, newScore);

        font.draw(sb, layout, (Asteroidz.WIDTH - layout.width) / 2, Asteroidz.HEIGHT / 3f + layout.height);

        for(int i = 0; i < newName.length; i++) {
            layout.setText(font, Character.toString(newName[i]));
            font.draw(sb, layout, (Asteroidz.WIDTH / 2 - layout.width) + 14 * i,Asteroidz.HEIGHT / 5f + layout.height);
        }


        String underscore = "_";
        layout.setText(font, underscore);
        font.draw(sb, layout, (Asteroidz.WIDTH / 2 - layout.width) + 15 * currentChar, Asteroidz.HEIGHT / 5f + layout.height);

        sb.end();

    }

    public void handleInput() {

        if(GameKeys.isPressed(GameKeys.ENTER)) {
            if(newHighScore) {
                Save.gd.addHighScore(
                        Save.gd.getTentativeScore(),
                        new String(newName)
                );
                Save.save();
            }
            gsm.setState(GameStateManager.MENU);
        }

        if(GameKeys.isPressed(GameKeys.UP)) {
            if(newName[currentChar] == ' ') {
                newName[currentChar] = 'Z';
            }
            else {
                newName[currentChar]--;
                if(newName[currentChar] < 'A') {
                    newName[currentChar] = ' ';
                }
            }
        }

        if(GameKeys.isPressed(GameKeys.DOWN)) {
            if(newName[currentChar] == ' ') {
                newName[currentChar] = 'A';
            }
            else {
                newName[currentChar]++;
                if(newName[currentChar] > 'Z') {
                    newName[currentChar] = ' ';
                }
            }
        }

        if(GameKeys.isPressed(GameKeys.RIGHT)) {
            if(currentChar < newName.length - 1) {
                currentChar++;
            }
        }

        if(GameKeys.isPressed(GameKeys.LEFT)) {
            if(currentChar > 0) {
                currentChar--;
            }
        }

    }

    public void dispose() {
        PlayState.removeInstance();
        MultiPlayState.removeInstance();
        sb.dispose();
        sr.dispose();
        gameOverFont.dispose();
        font.dispose();
        Jukebox.stop("gameover");
    }
}
