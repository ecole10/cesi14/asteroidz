package com.kelo.asteroidz.gamestates;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.kelo.asteroidz.Asteroidz;
import com.kelo.asteroidz.managers.GameKeys;
import com.kelo.asteroidz.managers.GameStateManager;
import com.kelo.asteroidz.managers.Jukebox;


public class PauseState extends GameState {

    public static int numberPlayers;

    public PauseState(GameStateManager gsm, int totalPlayers) {
        super(gsm, totalPlayers);
    }

    private SpriteBatch sb;
    public Asteroidz asteroidz;
    private BitmapFont titleFont;
    private BitmapFont font;
    private String[] menuItems;
    private int currentItem;

    @Override
    public void init() {

        sb = new SpriteBatch();

        FreeTypeFontGenerator gen = new FreeTypeFontGenerator(Gdx.files.internal("fonts/Hyperspace Bold.ttf"));

        FreeTypeFontGenerator.FreeTypeFontParameter parameterTitle = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameterTitle.size = Asteroidz.HEIGHT / 10;
        titleFont = gen.generateFont(parameterTitle);

        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = Asteroidz.HEIGHT / 30;
        font = gen.generateFont(parameter);

        menuItems = new String[] {
                "Resume",
                "Quit"
        };

        Jukebox.stopAll();


    }

    @Override
    public void update(float dt) {
        handleInput();
    }

    @Override
    public void draw() {
        sb.setProjectionMatrix(Asteroidz.cam.combined);

        sb.begin();

        GlyphLayout layout = new GlyphLayout();

        String title = "Pause";
        layout.setText(titleFont, title);
        titleFont.draw(sb, layout, (Asteroidz.WIDTH - layout.width) / 2, Asteroidz.HEIGHT / 1.5f);

        for(int i = 0; i < menuItems.length; i++) {
            if(currentItem == i) font.setColor(Color.RED);
            else font.setColor(Color.WHITE);
            layout.setText(font, menuItems[i]);
            font.draw(sb, layout, (Asteroidz.WIDTH - layout.width) / 2, (Asteroidz.HEIGHT / 2) - (layout.height + 20) * i);
        }

        sb.end();
    }

    @Override
    public void handleInput() {
        if(GameKeys.isPressed(GameKeys.UP)) {
            if(currentItem > 0) {
                currentItem--;
            }
        }
        if(GameKeys.isPressed(GameKeys.DOWN)) {
            if(currentItem < menuItems.length - 1) {
                currentItem++;
            }
        }
        if(GameKeys.isPressed(GameKeys.ENTER)) {
            select();
        }

    }

    private void select() {

        if (currentItem == 0) {

            if(numberPlayers > 1) {
                gsm.setState(GameStateManager.MULTIPLAY);
            } else {
                gsm.setState(GameStateManager.PLAY);
            }
        }
        if (currentItem == 1) {

            gsm.setState(GameStateManager.MENU);
        }
    }


    @Override
    public void dispose() {
        sb.dispose();
        font.dispose();
        titleFont.dispose();
        Jukebox.stopAll();
    }
}
