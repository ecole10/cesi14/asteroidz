package com.kelo.asteroidz.managers;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.kelo.asteroidz.entities.Player;
import com.kelo.asteroidz.gamestates.*;

import java.util.ArrayList;

public class GameStateManager {

    private GameState gameState;
    private ArrayList<Player> players;

    public static final int MENU = 0;
    public static final int PLAY = 1;
    public static final int MULTIPLAY = 2;
    public static final int HIGHSCORE = 3;
    public static final int CONTROLS = 4;
    public static final int OPTIONS = 5;
    public static final int GAMEOVER = 6;
    public static final int PAUSE = 7;

    public GameStateManager() {
        setState(MENU);
    }

    public void setState(int state) {
        if(gameState != null && !(gameState instanceof PlayState)) gameState.dispose();

        if(state == MENU) {
            gameState = new MenuState(this);
        }
        if(state == PLAY) {
            gameState = PlayState.getInstance(this);
        }
        if(state == MULTIPLAY) {
            gameState = MultiPlayState.getInstance(this);
        }
        if(state == HIGHSCORE) {
            gameState = new HighScoreState(this);
        }
        if(state == CONTROLS) {
            gameState = new ControlsState(this);
        }
        if(state == OPTIONS) {
            gameState = new OptionsState(this);
        }
        if(state == GAMEOVER) {
            gameState = new GameOverState(this);
        }
        if(state == PAUSE) {
            gameState = new PauseState(this, PlayState.getTotalPlayers());
        }
    }

    public void update(float dt) {
        gameState.update(dt);
    }

    public void update(float dt, ArrayList<Player> players) {
        gameState.update(dt, players);
    }

    public void draw() {
        gameState.draw();
    }

    public void draw(ArrayList<Player> players) {
        gameState.draw(players);
    }

    public void draw(ShapeRenderer sr, SpriteBatch sb, ArrayList<Player> players) {
        gameState.draw(sr, sb, players);
    }
}
