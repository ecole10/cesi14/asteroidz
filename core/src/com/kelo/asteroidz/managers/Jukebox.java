package com.kelo.asteroidz.managers;

import java.util.HashMap;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;

public class Jukebox {

    private static HashMap<String, Sound> sounds;
    private static HashMap<String, Music> musics;

    private static Float volumeSounds;
    private static Float volumeMusics;

    static {
        sounds = new HashMap<String, Sound>();
        musics = new HashMap<String, Music>();
    }

    public static void init(Float soundsVolume, Float musicsVolume) {

        if(soundsVolume != null) {
            volumeSounds = soundsVolume;
        } else {
            volumeSounds = 1f;
        }

        if(musicsVolume != null) {
            volumeMusics = musicsVolume;
        } else {
            volumeMusics = 1f;
        }

        for(Music m : musics.values()) {
            m.setVolume(volumeMusics);
        }
    }

    public static void load(String path, String name) {
        if(path.contains("sounds")) {
            Sound sound = Gdx.audio.newSound(Gdx.files.internal(path));
            sounds.put(name, sound);
        } else {
            Music music = Gdx.audio.newMusic(Gdx.files.internal(path));
            musics.put(name, music);
        }

    }

    public static void play(String name) {
        if(sounds.containsKey(name)) {
            sounds.get(name).play(volumeSounds);
        } else {
            musics.get(name).play();
        }
    }

    public static void loop(String name) {
        if(sounds.containsKey(name)) {
            sounds.get(name).loop(volumeSounds);
        } else {
            musics.get(name).setLooping(true);
        }
    }

    public static void stop(String name) {
        if(sounds.containsKey(name)) {
            sounds.get(name).stop();
        } else {
            musics.get(name).stop();
        }
    }

    public static void stopAll() {
        for(Sound s : sounds.values()) {
            s.stop();
        }

        for(Music m : musics.values()) {
            m.stop();
        }
    }

}
