package com.kelo.asteroidz.entities;

import java.util.ArrayList;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.*;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Polygon;
import com.kelo.asteroidz.Asteroidz;
import com.kelo.asteroidz.managers.Jukebox;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.utils.Array;
import java.util.HashMap;

public class FlyingSaucer extends SpaceObject {

    private ArrayList<Bullet> bullets;

    private int type;
    public static final int LARGE = 0;
    public static final int SMALL = 1;

    private int score;

    private float fireTimer;
    private float fireTime;

    private ArrayList<Player> players;

    private float pathTimer;
    private float pathTime1;
    private float pathTime2;

    private int direction;
    public static final int LEFT = 0;
    public static final int RIGHT = 1;

    private boolean remove;

    private TextureAtlas textureAtlas;
    final HashMap<String, Sprite> flyingSaucers = new HashMap<String, Sprite>();

    public FlyingSaucer(
            int type,
            int direction,
            ArrayList<Player> players,
            ArrayList<Bullet> bullets
    ) {

        this.type = type;
        this.direction = direction;
        this.players = players;
        this.bullets = bullets;
        speed = 70;

        textureAtlas = new TextureAtlas("sprites/flyingsaucers/flyingsaucers.txt");
        Array<AtlasRegion> regions = textureAtlas.getRegions();

        for (AtlasRegion region : regions) {
            Sprite sprite = textureAtlas.createSprite(region.name);

            flyingSaucers.put(region.name, sprite);

            if(type == 0 && region.name.contains("flying-saucer-small")) {
                circleBounds = new Circle(region.getRegionWidth(), region.getRegionHeight(), region.originalWidth / 2);
            }
            else if(type == 1 && region.name.contains("flying-saucer-large")) {
                circleBounds = new Circle(region.getRegionWidth(), region.getRegionHeight(), region.originalWidth / 2);
            }
        }

        if(direction == LEFT) {
            dx = -speed;
            x = Asteroidz.WIDTH;
        }
        else if(direction == RIGHT) {
            dx = speed;
            x = 0;
        }

        y = MathUtils.random(Asteroidz.HEIGHT);

        if(type == LARGE) {
            score = 200;
            Jukebox.loop("largesaucer");
        }
        else if(type == SMALL) {
            score = 1000;
            Jukebox.loop("smallsaucer");
        }

        fireTimer = 0;
        fireTime = 1;

        pathTimer = 0;
        pathTime1 = 2;
        pathTime2 = pathTime1 + 2;

    }

    public int getScore() { return score; }
    public boolean shouldRemove() { return remove; }

    public void update(float dt) {

        // fire
        for (Player player : players) {
            if (!player.isHit()) {
                fireTimer += dt;
                if (fireTimer > fireTime) {
                    fireTimer = 0;
                    if (type == LARGE) {
                        radians = MathUtils.random(2 * 3.1415f);
                    } else if (type == SMALL) {
                        radians = MathUtils.atan2(
                                player.gety() - y,
                                player.getx() - x
                        );
                    }
                    bullets.add(new Bullet(x, y, radians));
                    Jukebox.play("saucershoot");
                }
            }
        }

        // move along path
        pathTimer += dt;

        // move forward
        if(pathTimer < pathTime1) {
            dy = 0;
        }

        // move downward
        if(pathTimer > pathTime1 && pathTimer < pathTime2) {
            dy = -speed;
        }

        // move to end of screen
        if(pathTimer > pathTime1 + pathTime2) {
            dy = 0;
        }

        x += dx * dt;
        y += dy * dt;

        if(type == 0) {
            circleBounds.setPosition(x + 16, y + 16);
        }
        else if (type == 1) {
            circleBounds.setPosition(x + 32, y + 32);
        }

        // screen wrap
        if(y < 0) y = Asteroidz.HEIGHT;

        // check if remove
        if((direction == RIGHT && x > Asteroidz.WIDTH) ||
                (direction == LEFT && x < 0)) {
            remove = true;
        }

    }

    public void draw(SpriteBatch sb) {

        sb.begin();

        Sprite flyingSaucer = null;

        if(this.type < 0.5) {
            flyingSaucer = flyingSaucers.get("flying-saucer-small");
        } else {
            flyingSaucer = flyingSaucers.get("flying-saucer-large");
        }

        flyingSaucer.setPosition(x, y);
        flyingSaucer.draw(sb);

        sb.end();

    }

}
