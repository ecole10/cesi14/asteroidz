package com.kelo.asteroidz.entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.*;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Timer;
import com.kelo.asteroidz.Asteroidz;
import com.kelo.asteroidz.managers.Jukebox;
import java.util.ArrayList;
import java.util.HashMap;


public class Player extends SpaceObject {
	
	private final int MAX_BULLETS = 4;

	private final int INVINCIBLE = 0;
	private final int DESTROY = 1;

	private static final int FRAME_COLS = 3, FRAME_ROWS = 3;

	private float startX;
	private float startY;

	private SpriteBatch sb;

	private TextureAtlas textureAtlas;
	final HashMap<String, Sprite> spacecrafts = new HashMap<String, Sprite>();

	private ArrayList<Laser> lasers;

	private Sprite player;
	//	private Sprite die;

	private Animation<TextureRegion> explode;
	private Texture explodeSheet;
	float stateTime;

	private int killedFlyingSaucer;
	private int nextPower;
	private int power;
	private boolean usedPower;
	private boolean notAnymore;
	private boolean disableCollision;

	private boolean left;
	private boolean right;
	private boolean up;
	
	private float maxSpeed;
	private float acceleration;
	private float deceleration;
	private float acceleratingTimer;

	private boolean hit;

    private boolean dead;
	private float hitTimer;

    private float hitTime;
	private long score;

    private int extraLives;
    private long requiredScore;

	public Player() {
		startX = Asteroidz.WIDTH / 2;
		startY = Asteroidz.HEIGHT / 2;
		x = startX;
		y = startY;

		maxSpeed = 300;
		acceleration = 200;
		deceleration = 10;

		textureAtlas = new TextureAtlas("sprites/players/players.txt");
		Array<AtlasRegion> regions = textureAtlas.getRegions();

		for (AtlasRegion region : regions) {
			Sprite sprite = textureAtlas.createSprite(region.name);

			spacecrafts.put(region.name, sprite);

			circleBounds = new Circle(region.getRegionWidth(), region.getRegionHeight(), region.originalWidth / 2);
		}



		explodeSheet = new Texture(Gdx.files.internal("sprites/players/explodeSprite.png"));
		TextureRegion[][] tmp = TextureRegion.split(explodeSheet,
				explodeSheet.getWidth() / FRAME_COLS,
				explodeSheet.getHeight() / FRAME_ROWS);

		TextureRegion[] explodeFrames = new TextureRegion[FRAME_COLS * FRAME_ROWS];
		int index = 0;
		for (int i = 0; i < FRAME_ROWS; i++) {
			for (int j = 0; j < FRAME_COLS; j++) {
				explodeFrames[index++] = tmp[i][j];
			}
		}
		explode = new Animation<TextureRegion>(0.41f, explodeFrames);
		sb = new SpriteBatch();
		stateTime = 0f;

		radians = 3.1415f / 2;
		rotationSpeed = 3;

		hit = false;
		hitTimer = 0;
		hitTime = 2;

		player = spacecrafts.get("player");
		killedFlyingSaucer = 0;
		nextPower = 2;
		power = 9999;
		usedPower = false;
		notAnymore = false;

		score = 0;
		extraLives = 3;
		requiredScore = 5000;

	}

	public float getStartX() {
		return startX;
	}

	public float getStartY() {
		return startY;
	}

	public void setStartX(float startX) {
		this.startX = startX;
	}

	public void setStartY(float startY) {
		this.startY = startY;
	}

	public ArrayList<Laser> getLasers(){return this.lasers;}

	public void setLasers(ArrayList<Laser> lasers) {
		this.lasers = lasers;
	}

	public void setPlayer(Sprite player) {
		this.player = player;
	}

	public HashMap<String, Sprite> getSpacecrafts() {
		return spacecrafts;
	}

	public void setLeft(boolean b) { left = b; }
	public void setRight(boolean b) { right = b; }
	public void setUp(boolean b) {
		if(b && !up && !hit) {
			Jukebox.loop("thruster");
		}
		else if(!b) {
			Jukebox.stop("thruster");
		}
		up = b;
	}

	public void setPosition(float x, float y) {
		super.setPosition(x, y);
	}
	
	public boolean isHit() { return hit; }
	public boolean isDead() { return dead; }

	public void reset(float sx, float sy) {
		x = sx;
		y = sy;
		stateTime = 0f;
		hit = dead = false;
		if (usedPower()){resetPower();}
		disableCollision = true;
		Timer.schedule(new Timer.Task(){
			@Override
			public void run() {
				disableCollision = false;
			}
		}, 5 );
	}

	public boolean canUsePower() {

		if (killedFlyingSaucer > nextPower) {
			nextPower = getKilledFlyingSaucer();
		}
		if (notAnymore && !usedPower()) {
			notAnymore = false;
			return false;
		} else if (killedFlyingSaucer == nextPower && !usedPower()) {
			if (power > 1) {
				if (Math.random() < 0.5) {
					power = 0;
					Jukebox.play("power");
				} else {
					power = 1;
					Jukebox.play("power");
				}
				return true;
			} else if (power == 1){
				player = spacecrafts.get("player-destroy");
				player.setRotation(radians * MathUtils.radiansToDegrees - 90);
			}
			return true;
		}
		return false;
	}

	public int getNextPower() {
	    return nextPower;
    }

	public void incrementNextPower(int i) {
		this.nextPower += (killedFlyingSaucer + i);
	}

	public int getKilledFlyingSaucer() { return killedFlyingSaucer; }
	public long getScore() { return score; }
	public int getLives() { return extraLives; }

	public void loseLife() { extraLives--; }
	public void incrementKilledFlyingSaucer() { killedFlyingSaucer += 1; }
	public void incrementScore(long l) { score += l; }

	public void shoot() {
		if(lasers.size() == MAX_BULLETS) return;
		lasers.add(new Laser(x + (player.getWidth() / 2), y + (player.getHeight() / 2), radians));
		Jukebox.play("shoot");
	}

	public void powerInvincible() {
        player = spacecrafts.get("player-invincible");
		Jukebox.stop("pulsehigh");
		Jukebox.stop("pulselow");
		Jukebox.play("invincible");
		Timer.schedule(new Timer.Task(){
			@Override
			public void run() {
				player = spacecrafts.get("player");
				resetPower();
				Jukebox.stop("invincible");
			}
		}, 10 );
	}

	public boolean isDisableCollision() {
		return disableCollision;
	}

	public void isUsingPower(){
		usedPower = true;
	}

	public boolean usedPower() {
		return usedPower;
	}

	public int getPower() {
		return power;
	}

	public void resetPower() {
		this.notAnymore = true;
		this.usedPower = false;
		this.power = 9999;
	}

	public boolean usePower(int power) {
		// pass boolean value to playstate
		if (power == INVINCIBLE){
			isUsingPower();
			powerInvincible();
			return false;
		} else if (power == DESTROY) {
			isUsingPower();
			return true;
		}
		return false;
	}

	public void hit() {

		if(hit) return;

		hit = true;
		dx = dy = 0;
		left = right = up = notAnymore = false;
		Jukebox.stop("thruster");

	}

	public void die(float st){
		final TextureRegion currentFrame = explode.getKeyFrame(st, true);
		sb.begin();
		sb.draw(currentFrame, x - player.getWidth(), y - player.getHeight());
		sb.end();
	}

	public void update(float dt) {
		stateTime += dt;

		// check if hit
		if(hit) {
			hitTimer += dt;
			if(hitTimer > hitTime) {
				dead = true;
				hitTimer = 0;
			}
			die(stateTime);
			return;
		}
		
		// check extra lives
		if(score >= requiredScore) {
			extraLives++;
			requiredScore += 10000;
			Jukebox.play("extralife");
		}
		
		// turning
		if(left) {
			radians += rotationSpeed * dt;
		}
		else if(right) {
			radians -= rotationSpeed * dt;
		}
		
		// accelerating
		if(up) {
			dx += MathUtils.cos(radians) * acceleration * dt;
			dy += MathUtils.sin(radians) * acceleration * dt;
			acceleratingTimer += dt;
			if (!disableCollision) {
				if (power == 0 && usedPower()) {
					player = spacecrafts.get("player-move-invincible");
				} else if (power == 1) {
					player = spacecrafts.get("player-move-destroy");
				} else {
					player = spacecrafts.get("player-move");
				}
			} else {
				if (!usedPower()){
					player = spacecrafts.get("player-respawn");
				} else {
					if (power == 0){
						disableCollision = false;
						player = spacecrafts.get("player-invincible");
					}
				}
			}


			if(acceleratingTimer > 0.1f) {
				acceleratingTimer = 0;
			}
		}
		else {
			acceleratingTimer = 0;
			if (!disableCollision) {
				if (power == 0 && usedPower()) {
					player = spacecrafts.get("player-invincible");
				} else if (power == 1 && !usedPower()) {
					player = spacecrafts.get("player-destroy");
				} else {
					player = spacecrafts.get("player");
				}
			} else {
				if (!usedPower()){
					player = spacecrafts.get("player-respawn");
				} else {
					if (power == 0){
						disableCollision = false;
						player = spacecrafts.get("player-invincible");
					}
				}
			}

		}

		// deceleration
		float vec = (float) Math.sqrt(dx * dx + dy * dy);
		if(vec > 0) {
			dx -= (dx / vec) * deceleration * dt;
			dy -= (dy / vec) * deceleration * dt;
		}
		if(vec > maxSpeed) {
			dx = (dx / vec) * maxSpeed;
			dy = (dy / vec) * maxSpeed;
		}

		// set position
		x += dx * dt;
		y += dy * dt;


		player.setRotation(radians * MathUtils.radiansToDegrees - 90);
		player.setPosition(x, y);

		circleBounds.setPosition(x + 16, y + 16);
		
		// screen wrap
		wrap();
		
	}
	
	public void draw(SpriteBatch sb) {

		sb.begin();
		if (!hit && !dead) {
			player.draw(sb);
		}
		sb.end();
		
	}

}


















