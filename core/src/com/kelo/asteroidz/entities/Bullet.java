package com.kelo.asteroidz.entities;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Polygon;

public class Bullet extends SpaceObject {

    private float lifeTime;
    private float lifeTimer;

    private boolean remove;

    public Bullet(float x, float y, float radians) {

        this.x = x;
        this.y = y;
        this.radians = radians;

        shapex = new float[6];
        shapey = new float[6];

        float speed = 350;
        dx = MathUtils.cos(radians) * speed;
        dy = MathUtils.sin(radians) * speed;

        width = height = 2;

        lifeTimer = 0;
        lifeTime = 1;

        setShape();

        circleBounds = new Circle(x - width / 2,  y - width / 2, width / 2);

    }

    @Override
    public float[] getShapex() {
        return super.getShapex();
    }

    @Override
    public float[] getShapey() {
        return super.getShapey();
    }

    @Override
    public float[] getVertices(float[] shapex, float[] shapey) {
        return super.getVertices(shapex, shapey);
    }

    @Override
    public Polygon getPolyBounds() {
        return super.getPolyBounds();
    }

    @Override
    public Circle getCircleBounds() {
        return super.getCircleBounds();
    }

    public boolean shouldRemove() { return remove; }

    private void setShape() {
        shapex[0] = x + MathUtils.cos(radians) * 8;
        shapey[0] = y + MathUtils.sin(radians) * 8;

        shapex[1] = x + MathUtils.cos(radians - 5 * 3.1415f / 5) * 8;
        shapey[1] = y + MathUtils.sin(radians - 5 * 3.1415f / 5) * 8;

        shapex[2] = x + MathUtils.cos(radians - 3.1415f) * 5;
        shapey[2] = y + MathUtils.sin(radians - 3.1415f) * 5;

        shapex[3] = x - MathUtils.cos(radians) * 8;
        shapey[3] = y - MathUtils.sin(radians) * 8;

        shapex[4] = x - MathUtils.cos(radians - 5 * 3.1415f / 5) * 8;
        shapey[4] = y - MathUtils.sin(radians - 5 * 3.1415f / 5) * 8;

        shapex[5] = x - MathUtils.cos(radians - 3.1415f) * 5;
        shapey[5] = y - MathUtils.sin(radians - 3.1415f) * 5;

        polyBounds = new Polygon(getVertices(getShapex(), getShapey()));

    }

    public void update(float dt) {

        x += dx * dt;
        y += dy * dt;

        wrap();

        circleBounds.setPosition(x, y);


        lifeTimer += dt;
        if(lifeTimer > lifeTime) {
            remove = true;
        }

        setShape();

    }

    public void draw(ShapeRenderer sr) {
        sr.setColor(Color.WHITE);
        sr.begin(ShapeRenderer.ShapeType.Line);
        sr.circle(x - width / 2,  y - width / 2, width / 2);
        sr.end();
    }

}

