package com.kelo.asteroidz.entities;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.utils.Array;
import java.util.HashMap;

public class Asteroid extends SpaceObject {

    private int type;

    public static final int SMALL = 0;
    public static final int MEDIUM = 1;
    public static final int LARGE = 2;

    private TextureAtlas textureAtlas;
    final HashMap<String, Sprite> asteroids = new HashMap<String, Sprite>();

    private int score;

    protected boolean remove;

    public int getType() {
        return type;
    }

    public Asteroid(float x, float y, int type) {

        this.x = x;
        this.y = y;
        this.type = type;

        if (type == SMALL) {
            width = height = 12;
            speed = MathUtils.random(70, 100);
            score = 100;
        } else if (type == MEDIUM) {
            width = height = 20;
            speed = MathUtils.random(50, 60);
            score = 50;
        } else if (type == LARGE) {
            width = height = 40;
            speed = MathUtils.random(20, 30);
            score = 20;
        }

        textureAtlas = new TextureAtlas("sprites/asteroids/asteroids.txt");
        Array<AtlasRegion> regions = textureAtlas.getRegions();

        for (AtlasRegion region : regions) {
            Sprite sprite = textureAtlas.createSprite(region.name);

            asteroids.put(region.name, sprite);

            if(type == 0 && region.name.contains("asteroid-small")) {
                circleBounds = new Circle(region.getRegionWidth(), region.getRegionHeight(), region.originalWidth / 2);
            }
            else if(type == 1 && region.name.contains("asteroid-medium")) {
                circleBounds = new Circle(region.getRegionWidth(), region.getRegionHeight(), region.originalWidth / 2);
            }
            else if(type == 2 && region.name.contains("asteroid-large")) {
                circleBounds = new Circle(region.getRegionWidth(), region.getRegionHeight(), region.originalWidth / 2);
            }
        }

        rotationSpeed = MathUtils.random(-1, 1);

        radians = MathUtils.random(2 * 3.1415f);
        dx = MathUtils.cos(radians) * speed;
        dy = MathUtils.sin(radians) * speed;

    }

    public boolean shouldRemove() {
        return remove;
    }

    public int getScore() {
        return score;
    }

    public void update(float dt) {

        x += dx * dt;
        y += dy * dt;

        radians += rotationSpeed * dt;

        if(type == 0) {
            circleBounds.setPosition(x + 12, y + 12);
        }
        else if (type == 1) {
            circleBounds.setPosition(x + 18, y + 18);
        }
        else if (type == 2) {
            circleBounds.setPosition(x + 24, y + 24);
        }

        wrap();

    }

    public void draw(SpriteBatch sb) {

        sb.begin();

        Sprite asteroid = null;

        if(this.type == 0) {
            asteroid = asteroids.get("asteroid-small");
        }
        else if (type == 1) {
            asteroid = asteroids.get("asteroid-medium");
        }
        else if (type == 2) {
            asteroid = asteroids.get("asteroid-large");
        }

        asteroid.setPosition(x, y);
        asteroid.draw(sb);

        sb.end();

    }

}
