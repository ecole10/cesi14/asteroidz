package com.kelo.asteroidz.entities;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Polygon;
import com.kelo.asteroidz.Asteroidz;

import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;

public class SpaceObject {
	
	protected float x;
	protected float y;
	
	protected float dx;
	protected float dy;
	
	protected float radians;
	protected float speed;
	protected float rotationSpeed;
	
	protected int width;
	protected int height;
	
	protected float[] shapex;
	protected float[] shapey;
	protected Polygon polyBounds;
	protected Circle circleBounds;

	public float getx() { return x; }
	public float gety() { return y; }

	public float getdx() { return dx; }
	public float getdy() { return dy; }
	
	public float[] getShapex() { return shapex; }
	public float[] getShapey() { return shapey; }

	/* interlace two arrays
	 * input:
	 * float[] shapex = [1.0, 5.0, 9.0]
	 * float[] shapey = [3.0, 6.0, 7.0]
	 *
	 * output:
	 * float[] out = [1.0, 3.0, 5.0, 6.0, 9.0, 7.0]
	 */
	public float[] getVertices(float[] shapex, float[] shapey) {
		float[] out = new float[6];
		if (shapex != null && shapey != null) {
			out = new float[shapex.length + shapey.length];
			int j = 0;
			int maxLength = Math.max(shapex.length, shapey.length);
			for (int i = 0; i < maxLength; i++) {
				if (i < shapex.length) {
					out[j++] = shapex[i];
				}
				if (i < shapey.length) {
					out[j++] = shapey[i];
				}
			}
		}
		return out;
	}

	public Polygon getPolyBounds() {
		return this.polyBounds;
	}

	public Circle getCircleBounds() {
		return this.circleBounds;
	}

	public void setPosition(float x, float y) {
		this.x = x;
		this.y = y;
	}
	
	public boolean intersects(SpaceObject other) {
		return Intersector.overlaps(getCircleBounds(), other.getCircleBounds());
	}
	
	protected void wrap() {
		if(x < 0) x = Asteroidz.WIDTH;
		if(x > Asteroidz.WIDTH) x = 0;
		if(y < 0) y = Asteroidz.HEIGHT;
		if(y > Asteroidz.HEIGHT) y = 0;
	}

	public void freeze(float x, float y, int timeFreeze) {
		final float resumex = x;
		final float resumey = y;
		dx = dy = 0;

		Timer.schedule(new Task(){

		    @Override
            public void run() {
               dx = resumex;
               dy = resumey;
            }
		}, timeFreeze );
	}

	public void draw(ShapeRenderer sr) {}
	
}