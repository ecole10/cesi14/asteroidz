package com.kelo.asteroidz.entities;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

public class Laser extends Bullet {

    public Laser(float x, float y, float radians) {
        super(x, y, radians);
    }

    public void draw(ShapeRenderer sr) {
        sr.setColor(Color.WHITE);
        sr.begin(ShapeRenderer.ShapeType.Line);
        sr.polygon(getVertices(getShapex(), getShapey()));
        sr.end();
    }
}
