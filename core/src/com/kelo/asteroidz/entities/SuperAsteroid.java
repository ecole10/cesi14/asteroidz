package com.kelo.asteroidz.entities;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.kelo.asteroidz.Asteroidz;

public class SuperAsteroid extends Asteroid {

    private int timeFreeze;

    public SuperAsteroid(float x, float y, int type) {

        super(x, y, type);

        timeFreeze = 5;

    }

    public int getTimeFreeze() { return timeFreeze; }

    public void update(float dt) {

        x += dx * dt;
        y += dy * dt;

        radians += rotationSpeed * dt;

        circleBounds.setPosition(x + 18, y + 18);

        if(y < 0) y = Asteroidz.HEIGHT;

        if((x > Asteroidz.WIDTH) || (x < 0)) {
            remove = true;
        }

    }

    @Override
    public void draw(SpriteBatch sb) {

        sb.begin();

        Sprite asteroidFreeze = asteroids.get("asteroid-freeze");
        asteroidFreeze.setPosition(x, y);
        asteroidFreeze.draw(sb);

        sb.end();

    }

}
