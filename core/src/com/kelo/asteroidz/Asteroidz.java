package com.kelo.asteroidz;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.kelo.asteroidz.entities.Player;
import com.kelo.asteroidz.managers.GameInputProcessor;
import com.kelo.asteroidz.managers.GameKeys;
import com.kelo.asteroidz.managers.GameStateManager;
import com.kelo.asteroidz.managers.Jukebox;

import java.util.ArrayList;

public class Asteroidz extends ApplicationAdapter {

	public static int WIDTH;
	public static int HEIGHT;

	public static OrthographicCamera cam;

	private GameStateManager gsm;

	private static final int VIRTUAL_WIDTH = 1920;
	private static final int VIRTUAL_HEIGHT = 1080;
	private static final float ASPECT_RATIO =
			(float)VIRTUAL_WIDTH/(float)VIRTUAL_HEIGHT;

	private Rectangle viewport;
	private SpriteBatch sb;

	@Override
	public void create () {
		WIDTH = Gdx.graphics.getWidth();
		HEIGHT = Gdx.graphics.getHeight();
		sb = new SpriteBatch();

		cam = new OrthographicCamera(WIDTH, HEIGHT);
		cam.translate(WIDTH / 2, HEIGHT / 2);
		cam.update();

		Gdx.input.setInputProcessor(
				new GameInputProcessor()
		);

		Jukebox.load("sounds/explode.ogg", "explode");
		Jukebox.load("sounds/destroy.ogg", "destroy");
		Jukebox.load("sounds/extralife.ogg", "extralife");
		Jukebox.load("sounds/largesaucer.ogg", "largesaucer");
		Jukebox.load("sounds/pulsehigh.ogg", "pulsehigh");
		Jukebox.load("sounds/pulselow.ogg", "pulselow");
		Jukebox.load("sounds/saucershoot.ogg", "saucershoot");
		Jukebox.load("sounds/shoot.ogg", "shoot");
		Jukebox.load("sounds/smallsaucer.ogg", "smallsaucer");
		Jukebox.load("sounds/thruster.ogg", "thruster");
		Jukebox.load("sounds/invincible.ogg", "invincible");
		Jukebox.load("sounds/rewind.ogg", "rewind");
		Jukebox.load("sounds/gameover.ogg", "gameover");
		Jukebox.load("sounds/power.ogg", "power");
		Jukebox.load("musics/asteroids.ogg", "soundtrack");

		gsm = new GameStateManager();
	}

	@Override
	public void render () {

		cam.update();

		// set viewport
		Gdx.gl.glViewport((int) viewport.x, (int) viewport.y,
				(int) viewport.width, (int) viewport.height);

		// clear previous frame
		Gdx.gl.glClearColor(0,0,0,1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		try {
			gsm.update(Gdx.graphics.getDeltaTime());
			gsm.draw();
		} catch	(NullPointerException e){
			gsm.update(Gdx.graphics.getDeltaTime(), new ArrayList<Player>());
			gsm.draw(new ArrayList<Player>());
		}

		GameKeys.update();
	}

	@Override
	public void resize(int width, int height) {
		// calculate new viewport
		float aspectRatio = (float)width/(float)height;
		float scale = 1f;
		Vector2 crop = new Vector2(0f, 0f);if(aspectRatio > ASPECT_RATIO)
		{
			scale = (float)height/(float)VIRTUAL_HEIGHT;
			crop.x = (width - VIRTUAL_WIDTH*scale)/2f;
		}
		else if(aspectRatio < ASPECT_RATIO)
		{
			scale = (float)width/(float)VIRTUAL_WIDTH;
			crop.y = (height - VIRTUAL_HEIGHT*scale)/2f;
		}
		else
		{
			scale = (float)width/(float)VIRTUAL_WIDTH;
		}

		float w = (float)VIRTUAL_WIDTH*scale;
		float h = (float)VIRTUAL_HEIGHT*scale;
		viewport = new Rectangle(crop.x, crop.y, w, h);
	}

	@Override
	public void pause() {}
	@Override
	public void resume() {}
	@Override
	public void dispose () {
		sb.dispose();
	}
}
