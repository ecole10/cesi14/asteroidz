package com.kelo.asteroidz.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.kelo.asteroidz.Asteroidz;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();

		config.title= "Asteroidz";
		config.width = 1920;
		config.height = 1080;
		config.useGL30 = true;
		config.fullscreen = true;

		new LwjglApplication(new Asteroidz(), config);
	}
}