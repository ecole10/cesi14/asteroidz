# asteroidz by Kelo games

## Contraintes

    Mécanique quantique
    Reverse time
    Too much of a good thing

**MecaQ** : tir d'onde / explosion de particules

**Reverse time** : une astéroïd permet de ralentir les ennemis afin de les éviter facilement

**TMOAGT** : le joueur doit tuer un certain nombre d'ennemis. Une fois ce seuil atteint, il a accès à un "pouvoir" choisi aléatoirement (_invincibilité_ ou _destruction_ - ce dernier permet de passer directement au niveau supérieur après avoir détruit tous les ennemis)


## Details

[Asteroids](https://en.wikipedia.org/wiki/Asteroids_%28video_game%29) like

But : scorer

Fin : lorsque le joueur n'a plus de vies

## Compléments

Mode multijoueur
